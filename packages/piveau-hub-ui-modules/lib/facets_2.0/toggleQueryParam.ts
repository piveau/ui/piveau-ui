/**
 * Uses the vue router to perform the action of selecting a query param key-value pair.
 * If it already exists in the route it is removed, else it is added.
 * @param key
 * @param value
 * @param router
 * @param route
 */
export function toggleQueryParam(key, value, router, route) {
    let query: any = route.query[key];
    console.log("QUERY", query, typeof query)
    if ( ! query) {
        query = value;
    } else if (typeof query === 'string') {
        if (query === value) {
            query = undefined;
        } else {
            query = [query, value];
        }
    } else if (Array.isArray(query)) {
        if (query.includes(value)) {
            query = query.filter(element => element !== value);
        } else {
            query = [...query, value];
        }
    }
    router.push({
        path: route.path,
        query: {...route.query, [key]: query}
    });
}
