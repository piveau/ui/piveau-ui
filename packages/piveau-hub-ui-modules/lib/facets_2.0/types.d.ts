import {Ref} from "vue";
import {LocationQueryValue} from "vue-router";

export type FacetsConfigItem = {title: String, id: String, items: [], selected: string | null | LocationQueryValue[]};
export type FacetsConfig = [FacetsConfigItem];

export type FacetState = {config:FacetsConfigItem, selection:unknown};
export type FacetsState = [FacetState];
