import {FacetsConfig, FacetsConfigItem} from "./types";
import {Ref, ref, toValue, watchEffect} from "vue";
import {RouteLocationNormalizedLoadedGeneric} from "vue-router";

export function useFacets(facetsConfig: Ref<FacetsConfig>, facetsIds, route: RouteLocationNormalizedLoadedGeneric) {
    const facets = ref(null);
    const currentFacets = () => {
        facets.value = (facetsIds || []).reduce((acc, curr) => {
            const configItem: FacetsConfigItem = toValue(facetsConfig).find(c => c.id === curr);
            if (configItem) {
                // configItem.selected = route.query[configItem.id];
                acc.push(configItem);
            }
            return acc;
        }, []);
    };
    watchEffect(currentFacets);
    return facets;
}
