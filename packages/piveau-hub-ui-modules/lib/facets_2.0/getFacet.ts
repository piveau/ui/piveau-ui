import {Component} from "vue";
import {extras} from "../configurations/configureModules";
import ExpandableSelectFacet from "./facets/ExpandableSelectFacet.vue";
import DataServicesFacet from "./facets/DataServicesFacet.vue";
import RadioFacet from "./facets/RadioFacet.vue";

export const getFacet = (id: string): Component => {
    const custom = extras.customFacets;
    if (custom && custom[id]) { // custom
        return custom[id];
    } else { // defaults
        switch (id) {
            case "dataServices": return DataServicesFacet;
            case "superCatalog": return RadioFacet;
            // case "dataServices":case "superCatalog": return InfoFacet;
        }
        return ExpandableSelectFacet;
    }
}
