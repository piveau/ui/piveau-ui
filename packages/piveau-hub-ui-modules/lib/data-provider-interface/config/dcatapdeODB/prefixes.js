import prefixes from '../dcatapde/prefixes';

/**
 * Copy of vocabPrefixes of dcatapde spec
 */
const prefixesCopy = JSON.parse(JSON.stringify(prefixes));

export default prefixesCopy;