import formatTypes from '../dcatapde/format-types';

/**
 * Copy of formatTypes of DCAT-AP.de
 */
const formatTypesCopy = JSON.parse(JSON.stringify(formatTypes));

export default formatTypesCopy