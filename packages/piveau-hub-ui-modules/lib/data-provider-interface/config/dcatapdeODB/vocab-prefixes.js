import vocabPrefixes from "../dcatapde/vocab-prefixes";

/**
 * Copy of vocabPrefixes of dcatapde spec
 */
const vocabPrefixesCopy = JSON.parse(JSON.stringify(vocabPrefixes));

export default vocabPrefixesCopy