import pageContentConfig from '../dcatapde/page-content-config';

/**
 * Copy of pageContentConfig of dcatapde spec
 */
const pageContentConfigCopy = JSON.parse(JSON.stringify(pageContentConfig));

export default pageContentConfigCopy;