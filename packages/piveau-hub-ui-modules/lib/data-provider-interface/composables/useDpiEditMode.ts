import { useRoute } from "vue-router";
import { type DpiContext } from "./useDpiContext";
import { computed, MaybeRefOrGetter, toValue, watch } from "vue";
import { useStore } from "vuex";
import { useRuntimeEnv } from "../../composables/useRuntimeEnv";
import { useAsyncState, watchOnce } from "@vueuse/core";

  /**
   * Use this composable in the DataProviderInterface to fetch a dataset from the Hub API and
   * convert it to a form input via localStorage. This composable is used when the user navigates to a dataset
   * by clicking on the "Edit" on the DatasetDetails page or in DraftsPage
   *
   *
   * @param dpiContext - The DPI context as returned by `useDpiContext`.
   * @returns The computed properties described above.
   */
export function useDpiEditMode(dpiContext: MaybeRefOrGetter<DpiContext>) {
  const route = useRoute();
  const store = useStore();
  const env = useRuntimeEnv();

  const editQuery = computed(() => {
    return toValue(dpiContext)?.edit?.enabled ?? route.query.edit === 'true';
  });

  const editIdQuery = computed(() => {
    return toValue(dpiContext)?.edit?.id ?? route.query.id;
  });

  const editFromDraft = computed(() => {
    return toValue(dpiContext)?.edit?.fromDraft ?? route.query.fromDraft ?? store.getters["auth/getIsDraft"];
  });

  // For legacy purposes, set editmode to false if editQuery is false
  if (!!editQuery.value) {
    localStorage.setItem('dpi_editmode', 'false');
    store.dispatch("auth/setIsEditMode", false);
  }

  const requestParams = computed(() => {
    const isDraft = editFromDraft.value;
    const token = store.getters["auth/getUserData"]?.rtpToken;
    const property = route.params.property;
    const id = route.params.id;

    const endpoint = isDraft
      ? `${env.api.hubUrl}drafts/datasets/${editIdQuery.value}.nt?catalogue=${route.query.catalog}`
      : route.params.property === "catalogues"
      ? `${env.api.hubUrl}catalogues/${route.query.catalog}.nt`
      : `${env.api.hubUrl}datasets/${editIdQuery.value}.nt?useNormalizedId=true`;
    return { endpoint, token, property, id };
  });

  const { execute, isLoading, error } = useAsyncState(
    async () => {
      return await store.dispatch(
        "dpiStore/convertToInput",
        { ...requestParams.value }
      );
    },
    undefined,
    {
      immediate: false,
    }
  );

  const inEditModeAndRptAvailable = computed(() => !!editIdQuery.value && !!requestParams.value.token)
  watch(inEditModeAndRptAvailable, () => {
    if (!inEditModeAndRptAvailable.value) return;
    const isDraft = editFromDraft.value;
    store.dispatch("auth/setIsEditMode", true);
    store.dispatch("auth/setIsDraft", isDraft);
    execute();
  }, { immediate: true });

  // Ensure dpiStore contains a specification before rendering the input page.
  // Maybe it's not needed but better safe than sorry.
  const isReady = computed(() => {
    return !!store.getters["dpiStore/getSpecificationName"] && !isLoading.value;
  });

  return {
    isReady,
    error,
  }
}
